if ("undefined" == typeof(wdw_cardbooklog)) {
	try {
		ChromeUtils.import("resource://gre/modules/Services.jsm");
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("resource://gre/modules/Services.jsm");
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var wdw_cardbooklog = {

		getTime: function() {
			var objToday = new Date();
			var year = objToday.getFullYear();
			var month = ("0" + (objToday.getMonth() + 1)).slice(-2);
			var day = ("0" + objToday.getDate()).slice(-2);
			var hour = ("0" + objToday.getHours()).slice(-2);
			var min = ("0" + objToday.getMinutes()).slice(-2);
			var sec = ("0" + objToday.getSeconds()).slice(-2);
			var msec = ("00" + objToday.getMilliseconds()).slice(-3);
			return year + "." + month + "." + day + " " + hour + ":" + min + ":" + sec + ":" + msec;
		},

		updateStatusProgressInformation: function(aLogLine, aErrorType) {
			var statusInformationLength = cardbookPreferences.getStringPref("extensions.cardbook.statusInformationLineNumber");
			
			if (cardbookRepository.statusInformation.length >= statusInformationLength) {
				cardbookRepository.statusInformation.shift();
			}
			if (aErrorType) {
				cardbookRepository.statusInformation.push([wdw_cardbooklog.getTime() + " : " + aLogLine, aErrorType]);
			} else {
				cardbookRepository.statusInformation.push([wdw_cardbooklog.getTime() + " : " + aLogLine, "Normal"]);
			}
			// Services.console.logStringMessage(wdw_cardbooklog.getTime() + " : " + aLogLine.toSource());
		},

		updateStatusProgressInformationWithDebug1: function(aLogLine, aResponse) {
			if (aResponse) {
				var debugMode = cardbookPreferences.getBoolPref("extensions.cardbook.debugMode");
				if (debugMode) {
					wdw_cardbooklog.updateStatusProgressInformation(aLogLine + aResponse.toSource());
				}
			}
		},

		updateStatusProgressInformationWithDebug2: function(aLogLine) {
			var debugMode = cardbookPreferences.getBoolPref("extensions.cardbook.debugMode");
			if (debugMode) {
				wdw_cardbooklog.updateStatusProgressInformation(aLogLine);
			}
		}
	};

	var loader = Services.scriptloader;
	loader.loadSubScript("chrome://cardbook/content/preferences/cardbookPreferences.js");
};
