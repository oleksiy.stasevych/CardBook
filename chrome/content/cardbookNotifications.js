if ("undefined" == typeof(cardbookNotifications)) {
	try {
		ChromeUtils.import("chrome://cardbook/content/cardbookRepository.js");
	}
	catch(e) {
		Components.utils.import("chrome://cardbook/content/cardbookRepository.js");
	}

	var cardbookNotifications = {
		
		setNotification: function(aNotificationBoxId, aReasonCode, aValueArray, aPriority) {
			var notificationBox = document.getElementById(aNotificationBoxId);
			if (aReasonCode == "OK") {
				notificationBox.removeAllNotifications();
			} else {
				var aNotificationCode = aReasonCode;
				if (aValueArray && aValueArray.length > 0) {
					aNotificationCode = aNotificationCode + aValueArray.join(" ");
				}
				var existingBox = notificationBox.getNotificationWithValue(aNotificationCode);
				if (!existingBox) {
					notificationBox.removeAllNotifications();
					if (aValueArray && aValueArray.length > 0) {
						var myReason = cardbookRepository.strBundle.formatStringFromName(aReasonCode, aValueArray, aValueArray.length);
					} else {
						var myReason = cardbookRepository.strBundle.GetStringFromName(aReasonCode);
					}
					if (aPriority) {
						var myPriority = notificationBox[aPriority];
					} else {
						var myPriority = notificationBox.PRIORITY_WARNING_MEDIUM;
					}
					notificationBox.appendNotification(myReason, aNotificationCode, null, myPriority, null);
					notificationBox.getNotificationWithValue(aNotificationCode).setAttribute("hideclose", "true");
				}
			}
		}

	};
};
